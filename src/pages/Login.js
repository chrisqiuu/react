// modules
import styled from 'styled-components';
// import Navbar from '../components/Navbar';
import {useState, useEffect, useContext} from 'react';
import { Link } from 'react-router-dom';
import {Navigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

// start of CSS
const Container = styled.div`
    width: 100%;
    height: 92vh;
    background-color: #e1e1e1;
    display: flex;
    align-items: center;
    justify-content: center;
`

const Wrapper = styled.div`
    width: 25%;
    min-width: 300px;
    padding: 20px;
    text-align: center;
`

const Title = styled.h2`
    font-size: 1.5em;
    font-weight: 300;
    text-transform: uppercase;
`

const Form = styled.form`
    padding-right: 20px;
    margin-top: 20px;
`

const Input = styled.input`
    width: 100%;
    margin: 0 0 20px 0;
    padding: 10px;

    box-shadow: inset 3px 3px 6px #aaa, inset -2px -2px 5px #ccc;
    transition: all 0.2s ease-in-out;
    appearance: none;
    -webkit-appearance: none;
    border: 0;
    outline: 0;
    font-size: 1rem;
    border-radius: 320px;
    background-color: #ddd;
`

const Button = styled.button`
    width: 100%;
    border: none;
    padding: 15px 20px;
    margin: 20px 0 20px 10px;
    cursor: pointer;
    background-color: #696969;
    border-radius: 320px;

    color: #fff;
    box-shadow: -2px -2px 5px #c0c0c0,
    5px 5px 10px #000;
    transition: all 0.2s ease-in-out;

    &:hover {
        box-shadow: -2px -2px 5px #c0c0c0,
        1.5px 1.5px 5px #000;
    }

    &:active {
        box-shadow: inset 1px 1px 2px #c0c0c0,
        inset -1px -1px 2px #000,
    }
`

const Others = styled.div`
    margin-top: 10px;
    display: flex;
    flex-direction: column;
`

const ForgotPasswordLink = styled.a`
    color: crimson;
    cursor: pointer;
    margin-bottom: 10px;
    margin-left: 25px;
`

const RegisterLink = styled.div`
    margin-left: 25px;
`

const Anchor = styled.a`
    color: crimson;
    cursor: pointer;
`
// end of CSS



const Login = () => {
    const [username, setUsername] = useState ('');
    const [password, setPassword] = useState ('');
    const [isActive, setIsActive] = useState(false);

    //Allows us to consume the User context object and its properties to use for user validation
    const {user, setUser} = useContext(UserContext);

    useEffect(() => {
        if(username !== "" && password !== ""){	
            setIsActive(true);
        }else{
            setIsActive(false);
        }
    }, [username, password]);

    const loginUser = event => {
	    event.preventDefault();

        fetch(`${process.env.REACT_APP_URI}/api/user/login`, {
            method: "POST",
            headers: {
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({
                username: username,
                password: password
            })
            
        }).then(response => response.json())
        .then(data =>{
            //it is good to practice to always use or print out the result of our fetch request to ensure the correct information is received in our frontend application
            console.log(data);
    
            if(data.accessToken !== "empty"){
                localStorage.setItem('token', data.accessToken);
                retrieveUserDetails(data.accessToken);
                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome back!"
                })
            } else {
                Swal.fire({
                    title: "Login Failed",
                    icon: "error",
                    text: "Check your login credentials and try again."
                })
    
                setPassword('')
            }
        })

        const retrieveUserDetails = (token) => {
            fetch(`${process.env.REACT_APP_URI}/api/user/profile`,
                {headers: {
                    Authorization: `Bearer ${token}`
                }})
                .then(response=> response.json())
                .then(data => {
                    console.log(data);
                    
                    setUser({id: data._id, isAdmin: data.isAdmin, cart: data.cart})
                })	
        }
    }

    return (

        (user.id !== null) ?
        <Navigate to = "/" />:

             <Container>
                <Wrapper>
                    <Title>Sign In</Title>
                    <Form onSubmit={loginUser}>
                        <Input
                            placeholder="Username"
                            value={username}
                            onChange={event => setUsername(event.target.value)}
                            required/>
                        <Input
                            placeholder="Password"
                            type="password"
                            value={password}
                            onChange={event => setPassword(event.target.value)}
                            required/>
                        <Button type="submit" disabled= {!isActive}>Sign In</Button>
                        <Others>
                            <ForgotPasswordLink>Forgot Password?</ForgotPasswordLink>
                            <RegisterLink>
                                No account? <Anchor as={Link} to='/register'>Register</Anchor> here.
                            </RegisterLink>
                        </Others>
                    </Form>
                </Wrapper>
            </Container>
    )
}

export default Login