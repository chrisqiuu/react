// modules
import { useEffect, useState } from "react"
import styled from "styled-components"
import { mobile } from "../Responsive";
import Footer from "../components/Footer";
import AdminProducts from "../components/AdminProducts";
import { Link } from "react-router-dom";
// import axios from 'axios';

// start of CSS
// const Container = styled.div`
//     padding: 20px;
//     display: flex;
//     flex-wrap: wrap;
//     justify-content: space-between;
// `

const Container = styled.div`
`

const Wrapper = styled.div`
    padding: 20px;

    ${mobile({
        padding: "10px",
    })}
`

const Title = styled.h1`
    font-weight: 300;
    text-align: center;
`

const Top = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    gap: 50px;
    padding: 20px;
`

const TopButton = styled.button`
    padding: 10px;
    font-weight: 600;
    cursor: pointer;
    font-size: ${props => props.type === "unfilled" && "0.85em"};
    border: ${props => props.type === "filled" && "none"};
    background-color: ${props => props.type === "filled" ? "#000" : "transparent"};
    color: ${props => props.type === "filled" && "#fff"};
    padding: ${props => props.type === "filled" ? "10px 26.48px" : "8px 10px"};

    ${mobile({
        padding: "10px",
    })}
`

const Bottom = styled.div`
    display: flex;
    justify-content: space-between;

    ${mobile({
        flexDirection: "column",
    })}
`

const Info = styled.div`
    flex: 3;
`
//end of CSS

//{cat, filters, sort}
const AdminDashboard = () => {
  const [products, setProducts] = useState([]);
  const [isUpdated, setIsUpdated] = useState(false);

  useEffect(() => {

    fetch(`${process.env.REACT_APP_URI}/api/products/allProducts`, {
        headers: {
            'Content-Type' : 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
        },
    })
    .then(response => response.json())
    .then(data => {
        console.log(data);

        setProducts(data.map(product => {
            return (<AdminProducts key={product._id} product={product} setIsUpdated={setIsUpdated}/>)
        }))
        setIsUpdated(false);
    })

  }, [isUpdated]);

  return (
    <Container>
        <Wrapper>
            <Title>ALL PRODUCTS</Title>
            <Top>
                <TopButton as={Link} to="/" type="unfilled" style={{border: "2px solid #000", fontWeight: "500"}}>HOME</TopButton>

                <TopButton type="filled">VIEW STATISTICS</TopButton>
            </Top>
            <Bottom>
                <Info>
                    {products}
                </Info>
            </Bottom>
        </Wrapper>
        <Footer />
    </Container>
        
  )
}

export default AdminDashboard