//modules
import styled from "styled-components"
import Footer from '../components/Footer';
import { Add, Remove } from "@mui/icons-material";
import { mobile } from "../Responsive";
import { useContext, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import UserContext from "../UserContext";

// start of CSS
const Container = styled.div`

`

const Wrapper = styled.div`
    padding: 50px;
    display: flex;

    ${mobile({
        padding: "10px",
        flexDirection: "column",
    })}
`

const ImgContainer = styled.div`
    flex: 2;
    flex: ${props => props.type === "nonadmin" && "1"};
    display: flex;
    justify-content: center;
`

const Image = styled.img`
    width: 100%;
    height: 75vh;
    object-fit: cover;

    ${mobile({
        marginTop: "20px",
        width: "90%",
        height: "40vh",
    })}
`

const InfoContainer = styled.div`
    flex: 1;
    padding: 0 50px 0 100px;

    ${mobile({
        padding: "17px",
    })}
`

const Title = styled.h1`
    font-weight: 200;
    text-transform: uppercase;
`

const Desc = styled.p`
    margin: 20px 0;
`

const Price = styled.span`
    font-weight: 100;
    font-size: 2.5em;
`

const Category = styled.p`
    margin: 20px 0;
`

const Stock = styled.p`
    margin: 20px 0;
`

const AddToCartContainer = styled.div`
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: space-between;

    ${mobile({
        width: "100%",
    })}
`

const QuantityContainer = styled.div`
    display: flex;
    align-items: center;
    font-weight: 700;
    border: 0.5px solid #000;
    padding: 0 5px;
`

const Quantity = styled.span`
    width: 70px;
    height: 30px;
    border-left: 0.5px solid #000;
    border-right: 0.5px solid #000;
    display: flex;
    align-items: center;
    justify-content: center;
    margin: 0 5px;
`

const Hr = styled.hr`
    background-color: #eee;
    border: none;
    height: 1px;    
`

const EditFormWrapper = styled.div`
    width: 30%;
    margin: 0 auto;
    margin-bottom: 100px;

    ${mobile({
        textAlign: "center",
        marginTop: "30px",
        width: "70%",
    })}
`

const FormTitle = styled.h3`
    font-size: 1.8em;
    font-weight: 300;
    margin-bottom: 20px;
`

const Form = styled.form`
    display: flex;
    flex-direction: column;
`

const Label = styled.label`
    align-self: flex-start;
`

const Input = styled.input`
    height: 30px;
    width: 100%;
    margin: 10px auto;
`

const Button = styled.button`
    border: none;
    color: #fff;
    background-color: #000;
    padding: 7px 10px;
    margin-top: 10px;
    width: 40%;
    cursor: pointer;
    transition: all 0.5s ease;

    color: ${props => props.type === "cart" && "#000"};
    background-color: ${props => props.type === "cart" && "#fff"};
    font-weight: ${props => props.type === "cart" && "500"};
    border: ${props => props.type === "cart" && "2px solid #000"};
    padding: ${props => props.type === "cart" && "15px"};

    &:hover {
        color: #000;
        background-color: #527dff;
        color: ${props => props.type === "cart" && "#fff"};
        background-color: ${props => props.type === "cart" && "#000"};
    }
`
// end of CSS

const SingleProduct = () => {
    const {user, setIsCartUpdated: providerSetIsCartUpdated} = useContext(UserContext);
    const {productId} = useParams();
    const [quantity, setQuantity] = useState(1);
    const [isAdding, setIsAdding] = useState(false);

    const [item, setItem] = useState({
        title: '',
        description: '',
        price: 0,
        category: '',
        stock: 0,
        images: ''
    })

    const [fetchedItem, setFetchedItem] = useState({
        title: '',
        description: '',
        price: 0,
        category: '',
        stock: 0,
        images: ''
    })

    const [submitting, setSubmitting] = useState(false);
    const [updateSuccess, setUpdateSuccess] = useState(false)

    const handleInputChange = event => {
        const {value, name} = event.target
        console.log("@VALUE, NAME", value, name)

        setItem({
            ...item, 
            [name]:
                name === "price" || name === "stock"
                ?   parseInt(value)
                :   value
        })
    }

    console.log(item)

    const handleSubmit = event => {
        event.preventDefault();

        setSubmitting(true);
    }

    useEffect(() => {
        setSubmitting(false);

        const getItem = async () => {
            const result = await fetch(`${process.env.REACT_APP_URI}/api/products/${productId}`)
            console.log("@RESULT", result);
            
            const data = await result.json();
            console.log("@DATA", data);

            setUpdateSuccess(false);
            setFetchedItem(data);
            setItem({
                title: '',
                description: '',
                price: 0,
                category: '',
                stock: 0,
                images: ''
            })
        }

        getItem();

    }, [productId, updateSuccess])

    useEffect(() => {
        if(submitting) {
            const updateItem = async () => {
                const res = await fetch(`${process.env.REACT_APP_URI}/api/products/update/${productId}`, {
                    method: "PUT",
                    headers: {
                        'Content-Type' : 'application/json',
                        Authorization: `Bearer ${localStorage.getItem('token')}`
                    },
                    body: JSON.stringify(item)
                })

                console.log("@RES", res);
                if(res.status === 200) {
                    setUpdateSuccess(true);
                }
            }
            
            updateItem();
        }
    }, [submitting, item, productId])


    // for NON ADMIN and NON REGULAR USERS
    const handleQuantity = (type) => {
        if(type === "decrease") {
            quantity > 1 && setQuantity(quantity - 1);
        } else {
            setQuantity(quantity + 1);
        }
    }

    const handleAddToBag = () => {     
        console.log("@ADD", productId)

        setIsAdding(true);
    }

    // ADD TO CART
    useEffect(() => {
        if(isAdding) {
            const addToCart = async () => {
                const result = await fetch(`${process.env.REACT_APP_URI}/api/products/addtocart/${productId}`, {
                    method: "POST",
                    headers: {
                        'Content-Type' : 'application/json',
                        Authorization: `Bearer ${localStorage.getItem('token')}`
                    },
                    body: JSON.stringify({
					    quantity
                    })
                })

                console.log("@ADDTOCART", result);
                if(result.status === 200) {
                    setIsAdding(false);
                    providerSetIsCartUpdated(true);
                }
            }

            addToCart();
        }
    }, [isAdding, productId, quantity, providerSetIsCartUpdated])

    return (
        <>
            {
                (user !== null) && (user.isAdmin) &&
                <Container>
                    <Wrapper>
                        <ImgContainer>
                            <Image src={fetchedItem.images} />
                        </ImgContainer>
                        
                        <InfoContainer>
                            <Title>{fetchedItem.title}</Title>
                            <Desc>Description: {fetchedItem.description}</Desc>
                            <Price>PHP {fetchedItem.price}</Price>
                            <Category>Category: {fetchedItem.category}</Category>
                            <Stock>Stock: {fetchedItem.stock} pcs</Stock>
                        </InfoContainer>

                        <Hr />
                        <EditFormWrapper>
                            <FormTitle>Edit Product</FormTitle>
                                <Form onSubmit={handleSubmit}>
                                    <Label>Title</Label>
                                    <Input
                                        name="title"
                                        placeholder="Title"
                                        value={item.title}
                                        onChange={handleInputChange}
                                        required/>

                                    <Label>Description</Label>
                                    <Input
                                        name="description"
                                        placeholder="Description"
                                        value={item.description}
                                        onChange={handleInputChange}
                                        required/>   
                                        
                                    <Label>Price</Label>
                                    <Input
                                        type="number"
                                        name="price"
                                        placeholder="Price"
                                        value={item.price}
                                        onChange={handleInputChange}
                                        required/>

                                    <Label>Category</Label>
                                    <Input
                                        name="category"
                                        placeholder="Category"
                                        value={item.category}
                                        onChange={handleInputChange}
                                        required/>

                                    <Label>Upload Image</Label>
                                    <Input
                                        name="images"
                                        placeholder="Images"
                                        value={item.images}
                                        onChange={handleInputChange}
                                        required/>

                                    <Label>Stock</Label>
                                    <Input
                                        type="number"
                                        name="stock"
                                        placeholder="Stock"
                                        value={item.stock}
                                        onChange={handleInputChange}
                                        required/>
                                    
                                    <Button type="submit">Edit Product</Button>
                                </Form>    
                        </EditFormWrapper>
                    </Wrapper>            
                    <Footer />
                </Container>
            }
                
            {
                (user !== null || user === null) && (!user.isAdmin) &&
                <Container>
                    <Wrapper>
                        <ImgContainer type="nonadmin">
                            <Image type="nonadmin" src={fetchedItem.images} />
                        </ImgContainer>
                        
                        <InfoContainer type="nonadmin">
                            <Title>{fetchedItem.title}</Title>
                            <Desc>Description: {fetchedItem.description}</Desc>
                            <Price>PHP {fetchedItem.price * quantity}</Price>
                            <Category>Category: {fetchedItem.category}</Category>
                            <Stock>Stock: {fetchedItem.stock} pcs</Stock>

                            <AddToCartContainer>
                                <QuantityContainer>
                                    <Remove style={{cursor: "pointer"}} onClick={() => handleQuantity("decrease")}/>
                                    <Quantity>{quantity}</Quantity>
                                    <Add style={{cursor: "pointer"}} onClick={() => handleQuantity("increase")}/>
                                </QuantityContainer>
                                <Button type="cart" onClick={handleAddToBag}>ADD TO BAG</Button>
                            </AddToCartContainer>
                        </InfoContainer>
                        </Wrapper>
                    <Footer />
                </Container>
            }
        </>
    )
}

export default SingleProduct