// modules
import styled from "styled-components"
import Footer from '../components/Footer';
import Navbar from '../components/Navbar'
import { mobile } from "../Responsive";
import { useContext, useEffect, useState } from "react";
import CartItem from "../components/CartItem";
import UserContext from "../UserContext";

// start of CSS
const Container = styled.div`

`

const Wrapper = styled.div`
    padding: 20px;

    ${mobile({
        padding: "10px",
    })}
`

const Title = styled.h1`
    font-weight: 300;
    text-align: center;
`

const Top = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 20px;
`

const TopButton = styled.button`
    padding: 10px;
    font-weight: 600;
    cursor: pointer;
    border: ${props => props.type === "filled" && "none"};
    background-color: ${props => props.type === "filled" ? "#000" : "transparent"};
    color: ${props => props.type === "filled" && "#fff"};
    padding: ${props => props.type === "filled" ? "10px 26.48px" : "10px"};

    ${mobile({
        padding: "10px",
    })}
`

const TopTexts = styled.div`
    ${mobile({
        display: "none",
    })}
`

const ShoppingBag = styled.span`
    cursor: pointer;
    margin: 0 10px;
`
const Wishlist = styled.span`
    cursor: pointer;
    margin: 0 10px;
`

const Bottom = styled.div`
    display: flex;
    justify-content: space-between;

    ${mobile({
        flexDirection: "column",
    })}
`

const Info = styled.div`
    flex: 3;
`

const Hr = styled.hr`
    background-color: #eee;
    border: none;
    height: 1px;
`

const Summary = styled.div`
    flex: 1;
    border: 0.5px solid #000;
    padding: 20px;
    height: 50vh;
`

const SummaryTitle = styled.h1`
    font-weight: 200;
`

const SummaryItem = styled.div`
    margin: 30px 0;
    display: flex;
    justify-content: space-between;
    font-weight: ${props => props.type === "total" && "500"};
    font-size: ${props => props.type === "total" && "1.5em"};
`

const SummaryItemText = styled.span`

`

const SummaryItemPrice = styled.span`

`

const Button = styled.button`
    width: 100%;
    padding: 10px;
    color: #fff;
    background-color: #000;
    border: none;
    font-weight: 600;
    font-size: 1.5em;
    cursor: pointer;

    &:hover {
        color: #000;
        background-color: #6699ff;
    }
`
// end of CSS

const Cart = () => {
    const {user} = useContext(UserContext);
    const [cart, setCart] = useState([]);
    const [isCartUpdated, setIsCartUpdated] = useState(false);

    useEffect(() => {
        console.log("@AAAAAAA");
        fetch(`${process.env.REACT_APP_URI}/api/products/cart`, {
            headers: {
                'Content-Type' : 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
        })
        .then(response => {
            console.log("@RESPONSE", response);
            return response.json();
        })
        .then(data => {
            console.log("DATA@", data);
            setCart(data.map(cartItem => {
                return (<CartItem key={cartItem._id} cartItem={cartItem} setIsCartUpdated = {setIsCartUpdated} />)
            }))
        })

        setIsCartUpdated(false);

    }, [isCartUpdated]);

    return (
        <Container>
            <Wrapper>
                <Title>WHAT'S IN YOUR BAG</Title>
                <Top>
                    <TopButton>CONTINUE SHOPPING</TopButton>
                    <TopTexts>
                        <ShoppingBag>Shopping Bag: {cart.length}</ShoppingBag>
                        <Wishlist>Your Wishlist (0)</Wishlist>
                    </TopTexts>
                    <TopButton type="filled">CHECKOUT NOW</TopButton>
                </Top>
                <Bottom>
                    <Info>
                        {cart}
                    <Hr />
                    </Info>
                    <Summary>
                        <SummaryTitle>ORDER SUMMARY</SummaryTitle>
                        <SummaryItem>
                            <SummaryItemText>Subtotal</SummaryItemText>
                            <SummaryItemPrice>PHP 100</SummaryItemPrice>
                        </SummaryItem>
                        <SummaryItem>
                            <SummaryItemText>Shipping Fee</SummaryItemText>
                            <SummaryItemPrice>PHP 100</SummaryItemPrice>
                        </SummaryItem>
                        <SummaryItem>
                            <SummaryItemText>Discount</SummaryItemText>
                            <SummaryItemPrice>PHP 100</SummaryItemPrice>
                        </SummaryItem>
                        <SummaryItem type="total">
                            <SummaryItemText>Total</SummaryItemText>
                            <SummaryItemPrice>PHP 100</SummaryItemPrice>
                        </SummaryItem>
                        
                        <Button>CHECKOUT NOW</Button>
 
                    </Summary>
                </Bottom>
            </Wrapper>
            <Footer />
        </Container>
    )
}

export default Cart