// modules
import styled from 'styled-components';
import { Link, Navigate, useNavigate } from 'react-router-dom';
import React, { useEffect, useState } from 'react'
import Swal from 'sweetalert2';

// start of CSS
const Container = styled.div`
    width: 100%;
    height: 92vh;
    background-color: #e1e1e1;
    display: flex;
    align-items: center;
    justify-content: center;
`

const Wrapper = styled.div`
    width: 25%;
    min-width: 300px;
    padding: 20px;
    text-align: center;
`

const Title = styled.h2`
    font-size: 1.5em;
    font-weight: 300;
    text-transform: uppercase;
`

const Form = styled.form`
    margin-top: 20px;
    padding-right: 20px;
`

const Input = styled.input`
    width: 100%;
    margin: 0 0 20px 0;
    padding: 10px;

    box-shadow: inset 3px 3px 6px #aaa, inset -2px -2px 5px #ccc;
    transition: all 0.2s ease-in-out;
    appearance: none;
    -webkit-appearance: none;
    border: 0;
    outline: 0;
    font-size: 1rem;
    border-radius: 320px;
    background-color: #ddd;
`

const Button = styled.button`
    width: 100%;
    border: none;
    padding: 15px 20px;
    margin: 20px 0 20px 10px;
    cursor: pointer;
    background-color: #696969;
    border-radius: 320px;

    color: #fff;
    box-shadow: -2px -2px 5px #c0c0c0,
    5px 5px 10px #000;
    transition: all 0.2s ease-in-out;

    &:hover {
        box-shadow: -2px -2px 5px #c0c0c0,
        1.5px 1.5px 5px #000;
    }

    &:active {
        box-shadow: inset 1px 1px 2px #c0c0c0,
        inset -1px -1px 2px #000,
    }
`

const LoginPage = styled.p`
    font-size: 0.9em;
    margin-left: 20px;
`

const Login = styled.a`
    color: crimson;
    cursor: pointer;
`
// end of CSS

const Register = () => {
    const [name, setName] = useState("");
    const [username, setUsername] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [isActive, setIsActive] = useState(false);

    const navigate = useNavigate();

    useEffect(() => {
        name !== "" && username !== "" && email !== "" && password !== "" && confirmPassword !== "" && password === confirmPassword
        ? setIsActive(true)
        : setIsActive(false)

    }, [name, username, email, password, confirmPassword]);

    const registerUser = event => {
        event.preventDefault();

	    fetch(`${process.env.REACT_APP_URI}/api/user/register`, {
            method: "POST",
            headers: {
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({
                name,
                username,
                email,
                password: password,
                confirmPassword: confirmPassword
            })

        }).then(response => response.json())
        .then(data =>{
            console.log(data);

            if(data.usernameExists) {
                Swal.fire({
                    title: "Username Already Exists",
                    icon: "error",
                    text: "Please use another username"
                })

                setPassword('');
                setConfirmPassword('');
            } 

            else if(data.emailExists) {
                Swal.fire({
                    title: "Email Already Exists",
                    icon: "error",
                    text: "Please use another email"
                })

                setPassword('');
                setConfirmPassword('');
            } 

            else {
                Swal.fire({
                    title: "Registration Successful",
                    icon: "success",
                    text: "You may now login to our site!"
                })

                navigate('/login');
            }
        })
    }

    return (
        <Container>
            <Wrapper>
                <Title>Create Account</Title>
                <Form onSubmit={registerUser}>
                    <Input
                        type="text"
                        placeholder="Name"
                        value={name}
                        onChange={event => setName(event.target.value)}
                        required/>
                    <Input
                        type="text"
                        placeholder="Username"
                        value={username}
                        onChange={event => setUsername(event.target.value)}
                        required/>
                    <Input
                        type="email"
                        placeholder="Email"
                        value={email}
                        onChange={event => setEmail(event.target.value)}
                        required/>
                    <Input
                        placeholder="Password"
                        type="password"
                        value={password}
                        onChange={event => setPassword(event.target.value)}
                        required/>
                    <Input
                        placeholder="Confirm password"
                        type="password"
                        value={confirmPassword}
                        onChange={event => setConfirmPassword(event.target.value)}
                        required/>

                    <Button type="submit" disabled= {!isActive}>Create</Button>
                    <LoginPage>Already have an account? <Login as={Link} to='/login'>Click here to sign in</Login>.</LoginPage>
                </Form>
            </Wrapper>
        </Container>
  )
}

export default Register