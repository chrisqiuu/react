import React from 'react'
import { Link } from 'react-router-dom';

const NotFound = () => {
  return (
    <>
        <h1 className='h1-loader'>Error 404 | Page Not Found</h1>
        <div className="loader-wrapper  main-loader">
            <div className="loader-container">
            <span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span>

            <div className='loader-text'>
                Loading...
            </div>
            </div>
        </div>
        <p className='p-loader'>Go back to <Link to='/'>Home Page</Link>.</p>
    </>
  )
}

export default NotFound