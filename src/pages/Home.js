import React, { useContext } from 'react';
// import Navbar from '../components/Navbar';
import Footer from '../components/Footer';
import AllProducts from '../components/AllProducts';
import UserContext from '../UserContext';
import ActiveProducts from '../components/ActiveProducts';

const Home = () => {
  const {user} = useContext(UserContext);

  return (
    <div>
      {
        (user.isAdmin === true)
        ? <AllProducts />
        : <ActiveProducts />
      }
        <Footer />
    </div>
  )
}

export default Home