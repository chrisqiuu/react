import React, { useEffect, useContext } from 'react'
import { Navigate } from 'react-router-dom'
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

const Logout = () => {
    // window.location.href = '/login';

    const { setUser, unSetUser } = useContext(UserContext);
    
    unSetUser();

    useEffect(() => {
        setUser({id: null, isAdmin: false})
    }, [setUser])

    Swal.fire({
        title: "Thank you for shopping!",
        icon: "info",
        text: "Come back as many times as you want!"
    })

    return (
        <Navigate to='/login' />
    )
}

export default Logout