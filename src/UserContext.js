import React from 'react'

// createContext is a method used to create a context in react
const UserContext = React.createContext();

export const UserProvider = UserContext.Provider;

export default UserContext;