import { BrowserRouter as Router, Routes, Route, Navigate } from 'react-router-dom';
import './App.css';
import Home from './pages/Home';
import Login from './pages/Login';
import Register from './pages/Register';
import Navbar from './components/Navbar';
import Logout from './pages/Logout';
import CreateProduct from './pages/CreateProduct';
import { useEffect, useState } from 'react';
import { UserProvider } from './UserContext';
import AdminDashboard from './pages/AdminDashboard';
import ViewProduct from './pages/ViewProduct';
import Cart from './pages/Cart';
import Announcement from './components/Announcement';
import NotFound from './pages/NotFound';

function App() {
  const [user, setUser] = useState({id: null, isAdmin: false, cart: []});
  const [isCartUpdated, setIsCartUpdated] = useState(false);

  // function for clearing localStorage in the logout
  const unSetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_URI}/api/user/profile`,
			{headers: {
				Authorization: `Bearer ${localStorage.getItem("token")}`
			}})
			.then(response=> response.json())
			.then(data => {
				console.log(data);
				
				setUser({id: data._id, isAdmin: data.isAdmin, cart: data.cart})

			})	

      setIsCartUpdated(false);

  },[isCartUpdated])

  return (
    <>
      <UserProvider value={{user, setUser, unSetUser, setIsCartUpdated}}>
        <Router>
          <Navbar />
          <Announcement />
          <Routes>
            <Route path='/' exact element={<Home />} />
            <Route path='/login' exact element={user.id ? <Navigate to='/'/> : <Login />} />
            <Route path='/register' exact element={user.id ? <Navigate to='/'/> : <Register />} />
            <Route path='/logout' exact element={<Logout/>} />
            <Route path='/createProduct' exact element={<CreateProduct/>} />
            <Route path='/dashboard' exact element={<AdminDashboard />} />
            <Route path='/cart' exact element={<Cart />} />
            <Route path='/product/:productId' exact element={<ViewProduct />} />

            <Route path='*' exact element={<NotFound />} />

          </Routes>
        </Router>
      </UserProvider>
    </>
  );
}

export default App;