import styled from "styled-components"
import {mobile} from '../Responsive';

const Container = styled.div`
  height: 30px;
  background-color: #000;
  color: #fff;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 14px;
  font-weight: 500;
  transition: all 10s infinite;
  transform: translateX(${props => props.slideIndex * -100}vw);
`

const Announcement = () => {
  return (
    <Container>
        Announcement: NAWAY IPASA KAMI LAHAT NI SIR
    </Container>
  )
}

export default Announcement