// modules
import { Call, FacebookOutlined, Instagram, MailOutline, Place, Twitter } from "@mui/icons-material"
import styled from "styled-components"
import { mobile } from "../Responsive"

// start of CSS
const Container = styled.div`
    display: flex;
    background-color: #000;

    ${mobile({
        flexDirection: "column",
    })}
`

// start of Left contents
const Left = styled.div`
    flex: 1;
    display: flex;
    flex-direction: column;
    padding: 20px;

    ${mobile({
        textAlign: "center",
    })}
`

const Logo = styled.h1`
    color: #fff;
`

const Desc = styled.p`
    color: #fff;
    margin: 20px 0;
`

const Socials = styled.div`
    display: flex;

    ${mobile({
        justifyContent: "center",
    })}
`

const SocialIcon = styled.div`
    width: 40px;
    height: 40px;
    color: #fff;
    align-items: center;
    justify-content: center;
    margin-right: 20px;

    ${mobile({
        marginRight: "0",
        margin: "0 20px",
    })}
`
// end of Left contents

// start of Right contents
const Right = styled.div`
    flex: 1;
    padding: 20px;
`
const Title = styled.h3`
    margin-top: 20px;
    margin-bottom: 30px;
    color: #fff;
`

const ContactItem = styled.div`
    margin-bottom: 20px;
    display: flex;
    align-items: center;
    color: #fff;
    font-size: 1.2em;
`
// end of Right contents
// end of CSS

const Footer = () => {
  return (
    <Container>
        <Left>
            <Logo>추측하고 구매</Logo>
            <Desc>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Nihil, esse dolorem officia obcaecati praesentium sed quibusdam optio est, quo iste quis, nesciunt laudantium beatae in necessitatibus earum. Beatae, asperiores ad!</Desc>
            <Socials>
                <SocialIcon>
                    <FacebookOutlined style={{width: "40px", height: "40px"}}/>
                </SocialIcon>
                <SocialIcon>
                    <Instagram style={{width: "40px", height: "40px"}}/>
                </SocialIcon>
                <SocialIcon>
                    <Twitter style={{width: "40px", height: "40px"}}/>
                </SocialIcon>
            </Socials>
        </Left>
        <Right>
            <Title>Contact</Title>
            <ContactItem><Place style={{marginRight: "10px"}}/> 1234 Eskina Japan, Cebu City, Cebu</ContactItem>
            <ContactItem><Call style={{marginRight: "10px"}}/> +4 444 4444</ContactItem>
            <ContactItem><MailOutline style={{marginRight: "10px"}}/> edisapusomo@shop.io</ContactItem>
        </Right>
    </Container>
  )
}

export default Footer