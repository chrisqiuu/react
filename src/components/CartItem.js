// modules
import { Add, Remove } from "@mui/icons-material";
import { useContext, useEffect, useState } from "react";
import styled from "styled-components"
import { mobile } from "../Responsive";
import UserContext from "../UserContext";

// start of CSS
const Product = styled.div`
    display: flex;
    justify-content: space-between;
    margin: 10px 0;

    ${mobile({
        flexDirection: "column",
    })}
`

const ProductDetail = styled.div`
    flex: 2;
    display: flex;
`

const Image = styled.img`
    width: 150px;

    ${mobile({
        width: "177px",
    })}
`

const Details = styled.div`
    padding: 20px;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
`

const ProductName = styled.span``

const ProductId = styled.span``

const PriceDetail = styled.div`
    flex: 1;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;

    ${mobile({
        flexDirection: "row",
        marginTop: "10px",
        justifyContent: "space-around"
    })}
`

const ProductQuantityContainer = styled.div`
    display: flex;
    align-items: center;
    margin-bottom: 20px;
    border: 0.5px solid #000;
    padding: 0 5px;

    ${mobile({
        marginBottom: "0",
    })}
`

const ProductQuantity = styled.div`
    width: 70px;
    font-size: 1.5em;
    margin: 0 5px;
    border-right: 0.5px solid #000;
    border-left: 0.5px solid #000;
    display: flex;
    align-items: center;
    justify-content: center;
`

const ProductPrice = styled.div`
    font-size: 1.8em;
    font-weight: 200;
`

const Button = styled.button`
    margin-top: 25px;
    width: 130px;
    height: 40px;
    padding: 10px;
    color: #fff;
    background-color: #6699ff;
    border: none;
    font-weight: 600;
    font-size: 1em;
    cursor: pointer;

    &:hover {
        color: #000;
        background-color: #6699ff;
    }

    ${mobile({
        width: "110px",
    })}
`
// end of CSS

const CartItem = ({cartItem, setIsCartUpdated}) => {
    const [quantity, setQuantity] = useState(cartItem.quantity);
    const [checkout, setCheckout] = useState(false);
    const {setIsCartUpdated: providerSetIsCartUpdated} = useContext(UserContext);
    
    const handleQuantity = (type) => {
        if(type === "decrease") {
            quantity > 1 && setQuantity(quantity - 1);
        } else {
            setQuantity(quantity + 1);
        }
    }

    const handleCheckout = () => {
        setCheckout(true);
    }
    
    useEffect(() => {
        console.log("@CHECKOUT", checkout);

        if(checkout) {
            const checkoutItem = async () => {
                const result = await fetch(`${process.env.REACT_APP_URI}/api/products/checkout/${cartItem._id}`, {
                    method: "POST",
                    headers: {
                        'Content-Type' : 'application/json',
                        Authorization: `Bearer ${localStorage.getItem('token')}`
                    },
                })

                console.log(result);
                if(result.status === 200) {
                    setCheckout(false)
                    setIsCartUpdated(true);
                    providerSetIsCartUpdated(true)
                }
            }

            checkoutItem();
        }

    }, [checkout, cartItem, setIsCartUpdated, providerSetIsCartUpdated])


    return (
        <Product key={cartItem._id}>
            <ProductDetail>
                <Image src={cartItem.images}/>

                <Details>
                    <ProductName><b>Product:</b> {cartItem.title}</ProductName>
                    <ProductId><b>PRICE: </b>some price</ProductId>
                    <Button onClick={handleCheckout}>CHECKOUT</Button>
                </Details>
            </ProductDetail>

            <PriceDetail>
                <ProductQuantityContainer>
                    <Remove style={{cursor: "pointer"}} onClick={() => handleQuantity("decrease")}/>
                    <ProductQuantity>{cartItem.quantity}</ProductQuantity>
                    <Add style={{cursor: "pointer"}} onClick={() => handleQuantity("increase")}/>
                </ProductQuantityContainer>
                <ProductPrice>{`\u20B1 ${cartItem.amount * cartItem.quantity} `}</ProductPrice>
            </PriceDetail>
        </Product>
    )
}

export default CartItem