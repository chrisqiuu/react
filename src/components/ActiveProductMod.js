// modules
import styled from "styled-components"
import {FavoriteBorderOutlined, LocalMallOutlined, Search} from '@mui/icons-material';
import { Link } from "react-router-dom";

// start of CSS
const Info = styled.div`
    width: 100%;
    height: 100%;
    position: absolute;
    top: 0;
    left: 0;
    opacity: 0;
    background-color: rgba(0, 0, 0, 0.5);
    z-index: 3;
    display: flex;
    align-items: center;
    justify-content: center;
    transition: all 0.5s ease;
    cursor: pointer;
`

const Container = styled.div`
    flex: 1;
    margin: 5px;
    min-width: 300px;
    height: 450px;
    display: flex;
    align-items: center;
    justify-content: center;
    position: relative;

    &:hover ${Info} {
        opacity: 1;
    }
`

const Circle = styled.div`
    width: 200px;
    height: 200px;
    border-radius: 50%;
    background-color: #fff;
    position: absolute;
`

const Image = styled.img`
    width: 100%;
    height: 100%;
    object-fit: cover;
    z-index: 2;     
`

const Icon = styled.div`
    width: 40px;
    height: 40px;
    border-radius: 50%;
    background-color: #fff;
    display: flex;
    align-items: center;
    justify-content: center;
    margin: 10px;
    transition: all 0.5s ease;

    &:hover {
        background-color: #e9f5f5;
        transform: scale(1.5);
    }
`
//end of CSS

const ActiveProductMod = ({activeProduct}) => {
  return (
    <Container>
        <Circle />
        <Image src={activeProduct.images}/>
        <Info>
            <Icon>
                <LocalMallOutlined />
            </Icon>
            <Icon>
                <Link to={`/product/${activeProduct._id}`}>
                    <Search />
                </Link>
            </Icon>
            <Icon>
                <FavoriteBorderOutlined />
            </Icon>
        </Info>
    </Container>
  )
}

export default ActiveProductMod