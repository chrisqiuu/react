import React, { useContext } from 'react'
import { Badge } from '@mui/material'
import {Search, LocalMallOutlined} from '@mui/icons-material';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

// media-queries
import {mobile} from "../Responsive";
import UserContext from '../UserContext';

// start of CSS
const Container = styled.div`
    background-color: rgb(224, 229, 242);
`

const Wrapper = styled.div`
    padding: 10px 20px;
    display: flex;
    justify-content: space-between;
    align-items: center;

    ${mobile({
        padding: "10px 0px",
        flexDirection: "column",
    })}
`

// start of language and search box css
const Utilities = styled.div`
    flex: 1;
    display: flex;
    align-items: center;
    
    ${mobile({
        order: "1",
        margin: "5px 0 15px 0",
        width: "300px"
    })}
`

const Language = styled.span`
    font-size: 14px;
    cursor: pointer;

    ${mobile({
        display: "none",
    })}
`

const SearcBox = styled.div`
    border: 1px solid gray;
    display: flex;
    align-items: center;
    margin-left: 25px;
    padding: 5px;
    border-radius: 20px;

    ${mobile({
        marginLeft: "0",
        width: "100%",
    })}
`

const Input = styled.input`
    border: none;
    margin-left: 8px;
    background-color: rgb(224, 229, 242);

    ${mobile({
        width: "100%",
    })}
`
// end of language and search box css

// start of Logo css
const Logo = styled.div`
    flex: 1;

    ${mobile({
        fontSize: "8px",
        order: "0",
        margin: "5px 0"
    })}
`

const ShopName = styled.h1`
    text-align: center;
`
// end of Logo css

// start of Navbar menu css
const NavMenu = styled.div`
    flex: 1; 
    display: flex;
    align-items: center;
    justify-content: flex-end;

    ${mobile({
        flex: "2",
        justifyContent: "center",
        order: "2",
    })}
`

const Nav = styled.div`
    font-size: 14px;
    cursor: pointer;
    margin-left: 25px;

    ${mobile({
        fontSize: "12px",
        marginLeft: "0",
        margin: "0 40px"
    })}
`
// end of Navbar mernu css

const Navbar = () => {
    const {user} = useContext(UserContext);
    console.log(user)
    console.log("QWQEQWEQWEQWEQWEQEWQQEW")
    

    return (
        <Container>
            <Wrapper>
                <Utilities>
                    <Language>EN</Language>
                    <SearcBox>
                        <Input placeholder='Search'/>
                        <Search />
                    </SearcBox>
                </Utilities>
                <Link to="/">
                    <Logo><ShopName>추측하고 구매</ShopName></Logo>
                </Link>
                <NavMenu>
                    {
                        (user.id !== null)
                        ?   (user.isAdmin === true)
                        ?   <>
                            <Nav as={Link} to='/dashboard'>Dashboard</Nav>    
                            <Nav as={Link} to='/createProduct'>Create</Nav>    
                            <Nav as={Link} to='/logout'>Sign Out</Nav>  
                            </>
                        :   <>
                            <Nav as={Link} to='/products'>Shop</Nav>    
                            <Link to="/cart">
                                <Nav>
                                <Badge badgeContent={user.cart.length} color="primary">
                                    <LocalMallOutlined />
                                </Badge>
                                </Nav>    
                            </Link>  
                            <Nav as={Link} to='/logout'>Sign Out</Nav>  
                            </>
                        :   <>
                            <Nav as={Link} to='/products'>Shop</Nav>    
                            <Nav as={Link} to='/register'>Register</Nav>    
                            <Nav as={Link} to='/login'>Sign In</Nav>  
                            <Link to="/cart">
                                <Nav>
                                <Badge badgeContent={0} color="primary">
                                    <LocalMallOutlined />
                                </Badge>
                                </Nav>    
                            </Link>  
                            </> 
                    }
                </NavMenu>    
            </Wrapper>
        </Container>
    )
}

export default Navbar