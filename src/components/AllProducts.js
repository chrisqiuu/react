// modules
import { useEffect, useState } from "react"
import styled from "styled-components"
import AllProductMod from "./AllProductMod";
// import axios from 'axios';

// start of CSS
const Container = styled.div`
    padding: 20px;
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
`
//end of CSS

//{cat, filters, sort}
const Products = () => {
  const [products, setProducts] = useState([]);
//   const [filteredProducts, setFilteredProducts] = useState([]);

  useEffect(() => {
    // const getAllProducts = async () => {
    //   try {
    //     const res = await axios.get(
    //       cat
    //         ? `http://localhost:5000/api/products?category=${cat}`
    //         : "http://localhost:5000/api/products/"
    //     );
    //     setProducts(res.data);
    //   } catch (err) {}

    fetch(`${process.env.REACT_APP_URI}/api/products/allProducts`, {
        headers: {
            'Content-Type' : 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
        },
    })
    .then(response => response.json())
    .then(data => {
        console.log(data);

        setProducts(data.map(product => {
            return (<AllProductMod key={product._id} product={product}/>)
        }))
    })
        

    // };
    // getAllProducts();
  }, []);

//   useEffect(() => {
//     cat && setFilteredProducts(
//       products.filter(item => 
//         Object.entries(filters).every(([key,value]) =>
//          item[key].includes(value)
//         )
//       )
//     );
//   }, [products, cat, filters]);

//   useEffect(() => {
//     if (sort === "latest") {
//     //   console.log(23423432432);
//       setFilteredProducts((prev) => {
//         // console.log('latest products::', prev);
       
//         return [...prev].sort((a, b) =>{ 
//         //   console.log('original data of createdAt:', b.createdAt)
//         //   console.log('converted createdAt:', new Date(b.createdAt).getTime())
//           return new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime()})
//       });

//     } else if (sort === "lowToHigh") {
//       setFilteredProducts((prev) =>
//         [...prev].sort((a, b) => a.price - b.price)
//       );

//     } else if (sort === "highToLow") {
//       console.log(999999);
//       setFilteredProducts((prev) =>
//         [...prev].sort((a, b) => b.price - a.price)
//       );

//     } else {
//       setFilteredProducts((prev) => {
//         // console.log('latest products::', prev);

//         return [...prev].sort((a, b) =>{ 
//         //   console.log('original data of createdAt:', b.createdAt)
//         //   console.log('converted createdAt:', new Date(b.createdAt).getTime())
//             return new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime()})
//       });
//     }
//   }, [sort]);

  return (
    <Container>
        {products}
    </Container>
  )
}

export default Products

// {/* { cat
//           ? filteredProducts.map(product => <Product product={product} key={product._id} />)
//           : products.map(product => <Product product={product} key={product._id} /> )
//         } */}