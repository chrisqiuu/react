// modules
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import styled from "styled-components"
import { mobile } from "../Responsive";

// start of CSS
const Product = styled.div`
    width: 80%;
    display: flex;
    justify-content: space-between;
    margin: 20px auto;

    ${mobile({
        width: "100%",
        flexDirection: "column",
    })}
`

const ProductDetail = styled.div`
    flex: 2;
    display: flex;
`

const Image = styled.img`
    width: 150px;

    ${mobile({
        width: "177px",
    })}
`

const Details = styled.div`
    padding: 20px;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
`

const ProductName = styled.span`
    ${mobile({
        fontSize: "0.6em",
    })}
`

const ProductId = styled.span`
    ${mobile({
        fontSize: "0.6em",
    })}
`

const ProductStock = styled.span`
    ${mobile({
        fontSize: "0.6em",
    })}
`

const ProductStatus = styled.span`
    ${mobile({
        fontSize: "0.6em",
    })}
`

const PriceDetail = styled.div`
    flex: 1;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;

    ${mobile({
        flexDirection: "row",
        marginTop: "10px",
        justifyContent: "space-around",
        alignSelf: "flex-start"
    })}
`

const ProductQuantityContainer = styled.div`
    display: flex;
    align-items: center;
    margin-bottom: 20px;
    gap: 10px;

    ${mobile({
        marginBottom: "0",
    })}
`
const Button = styled.button`
    padding: 5px 10px;
    font-size: ${props => props.type === "link" && "0.8em"};
    border: none;
    color: #fff;
    background-color: #000;
    background-color: ${props => props.type === "activate" && "#527dff"};
    background-color: ${props => props.type === "deactivate" && "#3d5ec3"};
    font-weight: 600;
    cursor: pointer;
`

const Hr = styled.hr`
    background-color: #eee;
    border: none;
    height: 1px;
`
// end of CSS

const AdminProducts = ({product, setIsUpdated}) => {

    const [archive, setArchive] = useState(product.isActive);
    const [selectedId, setSelectedId] = useState("");

    // console.log(product._id);
    // console.log("@ARCHIVE", product);
    
    const handleArchive = productId => {
        console.log("ARCHIVE: ", productId)
        setArchive(!archive)
        setSelectedId(productId);
    }

    useEffect(() => {
        // console.log("@ARCHIVE:", archive)

        if(selectedId) {
            const archiveProduct = async () => {
                const res = await fetch(`${process.env.REACT_APP_URI}/api/products/archive/${selectedId}`, {
                    method: "PATCH",
                    headers: {
                        'Content-Type' : 'application/json',
                        Authorization: `Bearer ${localStorage.getItem('token')}`
                    },
                    body: JSON.stringify({
                        isActive: archive
                    })
                })
                console.log(res);
                if(res.status === 200) {
                    setIsUpdated(true);
                }
            }
    
            archiveProduct();
        }
        
    }, [archive, selectedId, setIsUpdated])

    return (
        <>  
         <Hr />
            <Product key={product._id}>
                <ProductDetail>
                    <Image src={product.images}/>
                    <Details>
                        <ProductName><b>Product:</b> {product.title}</ProductName>
                        <ProductId><b>ID:</b> {product._id}</ProductId>
                        <ProductId><b>Price:</b> PHP {product.price}</ProductId>
                        <ProductStock><b>Stock:</b> {product.stock} pcs</ProductStock>
                        {
                            product.isActive
                            ? <ProductStatus><b>Status:</b> <span style={{'color': 'blue'}}>ACTIVE</span></ProductStatus>
                            : <ProductStatus><b>Status:</b> <span style={{'color': 'gray'}}>NOT ACTIVE</span></ProductStatus>
                        }
                    </Details>
                </ProductDetail>
                <PriceDetail>
                    <ProductQuantityContainer>
                        {
                            product.isActive
                            ? <Button onClick={() => handleArchive(product._id)} type={"deactivate"}>Deactivate</Button>
                            : <Button onClick={() => handleArchive(product._id)} type={"activate"}>Activate</Button>
                        }
                        <Button as={Link} to={`/product/${product._id}`} type="link">Edit</Button>
                        <Button>Delete</Button>
                    </ProductQuantityContainer>
                </PriceDetail>
            </Product>
        </>

  
    )
}

export default AdminProducts